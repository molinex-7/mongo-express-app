var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectId;
var cod_scp = '<script src="';
cod_scp += 'http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js';
cod_scp += '"></script>';
cod_scp += '<script src="/javascripts/script.js"></script>';
var cod_scp_cor = '<script src="/javascripts/script_corrida.js"></script>';

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('main', { title: 'MyJsApp', script:'' });
});

/*** Motoristas ***/

router.get('/motoristas', function (req, res) {
	global.db.findAll('motoristas', (e, docs) => {
		if (e) { return console.log(e); }
		res.render('listaMotoristas', {title: 'Motoristas', docs: docs, script:'' });
	})
});

router.get('/motoristas-ajax', function(req,res,next) {
	global.db.findByActive('motoristas', (e, docs) => {
		if (e) { return console.log(e); }
		res.setHeader('Content-type', 'application/json');
		res.json(docs);
	})
});

router.get('/new-motoristas', function(req, res, next) {
  res.render('newMotorista', { title: 'Novo Motorista', script:cod_scp });
});

router.post('/new-motoristas', function (req, res) {
	var nome = req.body.nome;
	var nasc = req.body.nascimento;
	var cpf = req.body.cpf;
	var carro = req.body.carro;
	var sexo = req.body.sexo;
	var ativo = parseInt(req.body.ativo);

	global.db.insert('motoristas', {
		"nome": nome,
		"nascimento": nasc,
		"cpf": cpf,
		"carro": carro,
		"sexo": sexo,
		"ativo": ativo
	}, (err, result) => {
		if (err) { return console.log(err); }
		res.redirect('/motoristas');
	})
});

router.get('/edit-motorista/:id/:ativo', function(req, res,next) {
	var id = req.params.id;
	var act = (req.params.ativo == 1)? 0: 1;

	global.db.update('motoristas', id, act, (e, docs) => {
		if(e){ return console.log(e); }
		res.redirect('/motoristas');
	})
});

/*** Passageiros ***/

router.get('/passageiros', function(req,res,next) {
	global.db.findAll('passageiros', (e, docs) => {
		if (e) { return console.log(e); }
		res.render('listaPassageiros', {title: 'Passageiros', docs: docs, script:'' });
	})
});

router.get('/passageiros-ajax', function(req,res,next) {
	global.db.findAll('passageiros', (e, docs) => {
		if (e) { return console.log(e); }
		res.setHeader('Content-type', 'application/json');
		res.json(docs);
	})
});

router.get('/new-passageiros', function(req, res, next) {
  res.render('newPassageiro', { title: 'Novo Passageiro', script:cod_scp });
});

router.post('/new-passageiros', function (req, res) {
	var nome = req.body.nome;
	var nasc = req.body.nascimento;
	var cpf = req.body.cpf;
	var sexo = req.body.sexo;

	global.db.insert('passageiros', {
		"nome": nome,
		"nascimento": nasc,
		"cpf": cpf,
		"sexo": sexo
	}, (err, result) => {
		if (err) { return console.log(err); }
		res.redirect('/passageiros');
	})
});

/*** Corridas ***/

router.get('/corridas', function(req,res,next) {
	global.db.findAll('corridas', (e, docs) => {
		if (e) { return console.log(e); }
		res.render('listaCorridas', {title: 'Corridas', docs: docs, script:'' });
	})
});

router.get('/new-corridas', function(req, res, next) {
  res.render('newCorrida', { title: 'Nova Corrida', script:cod_scp_cor });
});

router.post('/new-corridas', function (req, res) {
	var mId = req.body.motorista_id;
	var mNome = req.body.motorista_nome;
	var pId = req.body.passageiro_id;
	var pNome = req.body.passageiro_nome;
	var valor = req.body.valor;

	global.db.insert('corridas', {
		"motorista": {
			"_id": new ObjectId(mId),
			"nome": mNome
		},
		"passageiro": {
			"_id": new ObjectId(pId),
			"nome": pNome
		},
		"valor": valor
	}, (err, result) => {
		if (err) { return console.log(err); }
		res.redirect('/corridas');
	})
});

module.exports = router;

(function () {
	//12345678909
	function TestaCPF(strCPF) {
		var Soma;
		var Resto;
		Soma = 0;
		
		if (strCPF == "00000000000") return false;

		for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;

		if ((Resto == 10) || (Resto == 11))  Resto = 0;
		if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

		Soma = 0;
		for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;

		if ((Resto == 10) || (Resto == 11))  Resto = 0;
		if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
		return true;
	}
	
	$('#datetimepicker10').datepicker({	format: 'dd M yyyy' });

	$('#myForm').submit(function (e) {
		e.preventDefault();

		var vazio = $('#myForm input').filter(function () {
			return !this.value;
		});

		if(vazio.length) {
			$('#myForm input').val('');
			alert('Ocorreu um erro. Por gentileza preencha de novo');
			return false;
		} else {
			if (TestaCPF($('input[name=cpf]').val())) {
				$('input[name=sexo]').val($('#sexo').val());
				this.submit();
			} else{
				$('#myForm input').val('');
				alert('Ocorreu um erro. Por gentileza preencha de novo');
				return false;
			}
		}
	});
})();


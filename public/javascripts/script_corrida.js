(function () {
	$(document).ajaxSend(function () {
		$('#myForm input[type=submit]').prop('disabled', true);
	});

	$.ajax({url: '/motoristas-ajax', success: function (response) {
		var items = [];
		$.each(response, function (index, element) {
			items.push('<option id="' + element._id + '">' + element.nome + '</option>');
		});

		$('#motorista').append(items);
	}});

	$.ajax({url: '/passageiros-ajax', success: function (response) {
		var items = [];
		$.each(response, function (index, element) {
			items.push('<option id="' + element._id + '">' + element.nome + '</option>');
		});

		$('#passageiro').append(items);
	}});

	$('#myForm').submit(function (e) {
		e.preventDefault();
		var mId = $('#motorista :selected').attr('id');
		var mNm = $('#motorista :selected').val();
		var pId = $('#passageiro :selected').attr('id');
		var pNm = $('#passageiro :selected').val();
		var valor = $('input[name=valor]').val();

		if (!mNm){ 
			alert('Não há motoristas ativos, ou cadastrados. Cadastre ou ative um primeiro'); 
			return false;
		}

		if (!pNm){ 
			alert('Não há passageiros ativos. Cadastre um primeiro'); 
			return false;
		}
		
		if (!valor || valor <= 0){ 
			alert('Não foi passado um valor. Defina um primeiro'); 
			return false;
		}

		$('input[name=motorista_id]').val(mId);
		$('input[name=motorista_nome]').val(mNm);
		$('input[name=passageiro_id]').val(pId);
		$('input[name=passageiro_nome]').val(pNm);

		this.submit();
	});
})();


var mongoClient  = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;

mongoClient.connect("mongodb://localhost:27017/app", { useNewUrlParser: true })
						.then(conn => global.conn = conn.db("app"))
						.catch(err => console.log(err))

function findAll(col, callback) {
	global.conn.collection(col).find({}).toArray(callback);
}

function insert(col, obj, callback) {
	global.conn.collection(col).insert(obj, callback);
}

function findOne(col, id, callback) {
	global.conn.collection(col).find(new ObjectId(id)).toArray(callback);
}

function update(col, id, act, callback) {
	global.conn.collection(col).update({_id:new ObjectId(id)}, {$set: {'ativo': act} }, callback);
}

function findByActive(col, callback) {
	global.conn.collection(col).find({ 'ativo': 1 }).toArray(callback);
}

module.exports = { findAll, insert, findOne, update, findByActive }

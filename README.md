# Mongo Express App

Made by Thiago Molina with technologies like MongoDB, Express, NodeJS, Atom (Just 
some parts. I can not abandon Vim).

In other words, a complete web application made with JS.

### Install

This time I will not teach how to install mongoDB, nor NodeJS, because these are 
easy (I'm lying, I'm just lazy), then with these two technologies installed, and
telling you that the NodeJS package that you installed already contains NPM...

Clone the repository, enter the directory and create the database with:

	$ mkdir data && mongod --dbpath ./data
	
### Tip

If you started the daemon, or if the package you downloaded started it, this 
command up there will not work, so do this before it:

    $ sudo systemctl stop mongodb

### Run it

To get the thing to work, do it.

    $ npm start

Access the http://localhost:3000/ and have fun...

See results:

![Screenshot](screenshots/shots-2018-07-31-22-27-13.png)

### Contact

For any clarification contact us

	Mail: t.molinex@gmail.com

